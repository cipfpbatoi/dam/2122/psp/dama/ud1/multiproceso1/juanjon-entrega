package psp.juanjo;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio3 {

    public static void main(String[] args) {

        String separator = System.getProperty("file.separator");
        String command = "java src" +separator+ "psp" +separator+ "juanjo" +separator+ "Minusculas.java";
        List<String> argsList = new ArrayList<>(Arrays.asList(command.split(" ")));

        /**
         * los archivos java no son ejecutables...
         */

        try {
            Process process = new ProcessBuilder(argsList).start();

            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner scannerProcess = new Scanner(process.getInputStream());
            Scanner sc = new Scanner(System.in);

            System.out.println("Ve escribiendo, el programa se detendrá cuando se introduzca 'finalizar'");
            String line = sc.nextLine();

            while (!line.equalsIgnoreCase("finalizar")) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                System.out.println(scannerProcess.nextLine());
                line = sc.nextLine();
            }

        } catch (IOException e) {
            System.err.println("ERROR: " +e.getMessage());
        }

    }

}
