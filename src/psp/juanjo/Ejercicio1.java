package psp.juanjo;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {

    public static void main(String[] args) {

        List<String> argsList = new ArrayList<>(List.of(args));
        ProcessBuilder pb = new ProcessBuilder(argsList);

        try {
            Process process = pb.start();
            if (!process.waitFor(2, TimeUnit.SECONDS)) {
                System.out.println("El programa duró mas tiempo del esperado");
            } else {
                /**
                 * No se comprueba si el programa finalizó correctamente...
                 */
                InputStreamReader isr = new InputStreamReader(process.getInputStream());
                BufferedReader br = new BufferedReader(isr);
                String line;
                FileWriter fw = new FileWriter(Paths.get("output.txt").toFile());
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                    fw.write(line + "\n");
                    fw.flush();
                }
            }
        } catch (IOException | InterruptedException e) {
            System.err.println("ERROR: " + e.getMessage());
        }

    }

}
