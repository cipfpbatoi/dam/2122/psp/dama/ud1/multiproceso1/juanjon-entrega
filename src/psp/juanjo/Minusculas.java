package psp.juanjo;

import java.util.Scanner;

public class Minusculas {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String line = sc.nextLine();
            line = line.toLowerCase();
            if (!line.equals("finalizar")) {
                System.out.println(line);
            } else {
                System.exit(1);
            }
        }

    }

}
