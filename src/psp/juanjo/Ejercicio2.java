package psp.juanjo;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio2 {

    public static void main(String[] args) {

        String separator = System.getProperty("file.separator");
        String command = "java src" +separator+ "psp" +separator+ "juanjo" +separator+ "Ramdom10.java";
        List<String> argsList = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(argsList);

        /**
         * No se guarda en el fichero
         */

        try {
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            Scanner scannerProcess = new Scanner(process.getInputStream());
            Scanner sc = new Scanner(System.in);

            Path p = Paths.get("randoms.txt");
            if (Files.notExists(p)) {
                Files.createFile(p);
            }

            System.out.println("Ve escribiendo, el programa se detendrá cuando se introduzca 'stop'");
            String line = sc.nextLine();
            String number;
            List<String> numbers = new ArrayList<>();

            while (!line.equalsIgnoreCase("stop")) {
                bw.write(line);
                bw.newLine();
                bw.flush();
                number = scannerProcess.nextLine();
                System.out.println(number);
                line = sc.nextLine();
                numbers.add(number);
            }
            Files.write(p, numbers);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

    }

}
